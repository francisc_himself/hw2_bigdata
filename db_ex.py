#!/usr/bin/python
import  sqlite3
from pprint import pprint
import argparse
import sys

# 1 Connect to the databases
conn_reuters = sqlite3.connect('reuters.db')
print "[info:] Reuters DB connected successfully!"
conn_matrix = sqlite3.connect('matrix.db')
print "[info:] Matrix DB connected successfully!\n"

# Function that only prints first entry of SQL query result row
def print_query_result(cursor):
    for row in cursor:
        print row[0]


class color:
    BOLD = '\033[1m'
    END = '\033[0m'

# for no output, use 0 as second argument, and whatever, elsewise
def get_sparse_matrix_from_db(mat_name, cursor_matrix, output):
    mat_dict = dict()
    query_string = "SELECT row_num, col_num, value FROM "+mat_name
    query_max_row = int(cursor_matrix.execute("SELECT MAX(row_num) FROM "+mat_name).fetchone()[0])
    query_max_col = int(cursor_matrix.execute("SELECT MAX(col_num) FROM "+mat_name).fetchone()[0])

    mat_std = [[0 for i in range(query_max_row+1)] for j in range(query_max_row+1)]

    cursor_matrix.execute(query_string)

    # Store as DOK (Dictionary of Keys)
    # that maps (row, column) -> value
    sparse_mat_dict = dict()

    for row_num, col_num, value in cursor_matrix:
        sparse_mat_dict[row_num, col_num] = value


    for i in range(int(query_max_row + 1)):
        for j in range(int(query_max_col + 1)):
            if output:
                print [i, j],
            if tuple([i, j]) in sparse_mat_dict.keys():
                if output:
                    print color.BOLD + str(sparse_mat_dict[tuple([i, j])]) + color.END,
                mat_dict[i, j] = sparse_mat_dict[tuple([i, j])]
                mat_std[i][j] = sparse_mat_dict[tuple([i, j])]
            else:
                if output:
                    print "0",
                mat_dict[i, j] = 0
        if output:
            print "\n"
    if output:
        print "Your NON-sparse matrix as a dict:"
        pprint(mat_dict)
    return mat_std
def mul_sql_matrices(mat1_name, mat2_name):
    query_max_row = cursor_matrix.execute("SELECT MAX(row_num) FROM " + mat1_name).fetchone()[0]
    query_max_col = cursor_matrix.execute("SELECT MAX(col_num) FROM " + mat1_name).fetchone()[0]

    # Matrix multiplication approach with SQL) A[i, k] * B[k, j] = C[i, j]
    query_string =                                                                                          \
        "SELECT "+mat1_name+".row_num, "+mat2_name+".col_num, SUM("+mat1_name+".value*"+mat2_name+".value)" \
        " FROM "+mat1_name+", "+mat2_name  +                                                                \
        " WHERE "+mat1_name+".col_num = "+mat2_name+".row_num"                                              \
        " GROUP BY "+mat1_name+".row_num, "+mat2_name+".col_num;"
    cursor_matrix.execute(query_string)

    # Store as DOK (Dictionary of Keys)
    # that maps (row, column) -> value
    sparse_mat_dict = dict()
    mat_dict = dict()
    for row_num, col_num, value in cursor_matrix:
        sparse_mat_dict[row_num, col_num] = value

    for i in range(int(query_max_row + 1)):
        for j in range(int(query_max_col + 1)):
            print [i, j],
            if tuple([i, j]) in sparse_mat_dict.keys():
                print color.BOLD + str(sparse_mat_dict[tuple([i, j])]) + color.END,
                mat_dict[i, j] = sparse_mat_dict[tuple([i, j])]
            else:
                print "0",
                mat_dict[i, j] = 0
        print "\n"
    print_query_result(cursor_matrix)
def mul_py_matrices(mat1_name, mat2_name):
    query_max_row = int(cursor_matrix.execute("SELECT MAX(row_num) FROM " + mat1_name).fetchone()[0])
    query_max_col = int(cursor_matrix.execute("SELECT MAX(col_num) FROM " + mat2_name).fetchone()[0])

    mat_a = get_sparse_matrix_from_db("a", cursor_matrix, 0)
    print "1'st MAT:"
    pprint(mat_a)

    print "\n"
    mat_b = get_sparse_matrix_from_db("b", cursor_matrix, 0)
    print "2'nd MAT:"
    pprint(mat_b)
    print "\n"

    result = [[0 for i in range(query_max_row + 1)] for j in range(query_max_col + 1)]

    # iterate through rows of a
    for i in range(len(mat_a)):
        # iterate through columns of b
        for j in range(len(mat_b[0])):
            # iterate through rows of b
            for k in range(len(mat_b)):
                result[i][j] += mat_a[i][k]*mat_b[k][j]
    print "RES:"
    for r in result:
        print(r)

# Program starts here
## Program begins here

parser = argparse.ArgumentParser(description="lab2 BIG data")
parser.add_argument ("-req1", action='store_true', help="SQL:count 'net'-containing documents", required=False)
parser.add_argument ("-req2", action='store_true', help="SQL:docs with over 300 words", required=False)
parser.add_argument ("-req3", action='store_true', help="SQL:docs containing 'transactions' and 'world'", required=False)
parser.add_argument ("-req4", action='store_true', help="Py:retrieve sparse matrices from SQL", required=False)
parser.add_argument ("-req5", action='store_true', help="Py:mat mul in py using res from req4", required=False)
parser.add_argument ("-req6", action='store_true', help="SQL:sparse mat mul", required=False)
parser.add_argument ("-req7", action='store_true', help="SQL:similarity of 10080_txt_crude and 17035_txt_earn", required=False)
parser.add_argument ("-req8", action='store_true', help="SQL:match document to 'washington taxes treasury'", required=False)

args = parser.parse_args()

cursor_reuters = conn_reuters.cursor()
cursor_matrix = conn_matrix.cursor()

if len(sys.argv[1:])==0:
    parser.print_help()

# # Query 1:
if args.req1:
    cursor_reuters.execute("SELECT count(docid) FROM Frequency WHERE term = 'net'")
    print "Query 1: Number of documents containing the word \"net\""
    print_query_result(cursor_reuters)
    print "\n"

# # Query 2:
if args.req2:
    cursor_reuters.execute("SELECT docid, sum(count) FROM Frequency GROUP BY docid HAVING sum(count)>300")
    print "Query 2: Documents that contain the word \"net\" in over 300 occurences"
    print_query_result(cursor_reuters)
    print  "\n"

# # Query 3:
if args.req3:
    print  "Query 3: Unique documents that contain 'transactions' as well as 'world'"
    query_string =                                           \
        " SELECT *"                                          \
        " FROM Frequency f1"                                 \
        " WHERE EXISTS ("                                    \
        " SELECT *"                                          \
        " FROM Frequency f2"                                 \
        " WHERE f1.docid = f2.docid"                         \
        " AND f1.term='world' and f2.term='transactions')"

    cursor_reuters.execute(query_string)
    print_query_result(cursor_reuters)
    print  "\n"


# # Query 4:
if args.req4:
    print "Query 4: read a matrix from DB"
    get_sparse_matrix_from_db("a", cursor_matrix, "output=yes")
    print "\n"
    get_sparse_matrix_from_db("b", cursor_matrix, "output=yes")

# # Query 5:
if args.req5:
    mul_py_matrices("a", "b")

# # Query 6:
if args.req6:
    print "Query 6: SQL matrix multiplication: Result of multiplication: \n"
    mul_sql_matrices("a", "b")

# # Query 7:
if args.req7:
    # Proposed solution: 1) take two instances of the Table Frequency, freq1 and freq2
    #                    2) apply sparse mat mul with transposed (D*DT)
    print "Query 7: SQL similarity of 10080_txt_crude and 17035_txt_earn"
    query_string =                                                         \
    "select sum(freq1.count * freq2.count)"                                \
    " FROM Frequency freq1, Frequency freq2"                               \
    " WHERE freq1.term = freq2.term AND"                                   \
    " freq1.docid = '10080_txt_crude' and freq2.docid = '17035_txt_earn'"
    cursor_reuters.execute(query_string)
    print "\nRESULT >>> ",
    print_query_result(cursor_reuters)

if args.req8:
    print "Query 8: match document to 'washington taxes treasury'"
    # 1) CREATE VIEW
    query_string_1 = "CREATE VIEW IF NOT EXISTS keyword AS"+\
                        " SELECT * FROM frequency"+\
                        " UNION"+\
                        " SELECT 'q' as docid, 'washington' as term, 1 as count"+\
                        " UNION"+\
                        " SELECT 'q' as docid, 'taxes' as term, 1 as count"+\
                        " UNION"+\
                        " SELECT 'q' as docid, 'treasury' as term, 1 as count"
    cursor_reuters.execute(query_string_1)

    # 2) perform op
    query_string_2 = "SELECT A.docid, sum(A.count*B.count) AS SCORE"\
                     " FROM keyword A, keyword B WHERE A.term = B.term"\
                     " AND B.docid='q'" \
                     " GROUP BY A.docid order by -score limit 10"
    cursor_reuters.execute(query_string_2)
    for row in cursor_reuters:
        print row[0], "->[score:]", row[1]